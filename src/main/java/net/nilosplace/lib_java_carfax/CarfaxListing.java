package net.nilosplace.lib_java_carfax;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class CarfaxListing {

	private String vin;
	private String year;
	private Integer mileage;
	private Integer listPrice;
	private Integer currentPrice;
	
	public String getLink() {
		return "Link: https://www.carfax.com/vehicle/" + vin;
	}
}

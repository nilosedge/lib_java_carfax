package net.nilosplace.lib_java_carfax;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/api")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
public interface CarfaxRestInterface {
	
	@GET
	@Path("/vehicles")
	public CarfaxSearchResults vehicleSearch(
		@HeaderParam("accept") String accept,
		@HeaderParam("accept-encoding") String acceptEncoding,
		@HeaderParam("accept-language") String acceptLanguage,
		@HeaderParam("cache-control") String cacheControl,
		@HeaderParam("pragma") String pragma,
		@HeaderParam("sec-fetch-dest") String secFetchDest,
		@HeaderParam("sec-fetch-mode") String secFetchMode,
		@HeaderParam("sec-fetch-site") String secFetchSite,
		@HeaderParam("sec-fetch-user") String secFetchUser,
		@HeaderParam("upgrade-insecure-requests") Integer upgradeInsecureRequests,
		@HeaderParam("user-agent") String userAgent,
		@QueryParam("zip") String zip,
		@QueryParam("radius") Integer radius,
		@QueryParam("make") String make,
		@QueryParam("model") String model,
		@QueryParam("noAccidents") Boolean noAccidents,
		@QueryParam("oneOwner") Boolean oneOwner,
		@QueryParam("page") Integer page
	);

}

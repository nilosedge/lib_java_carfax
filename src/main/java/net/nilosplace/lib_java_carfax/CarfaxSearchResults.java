package net.nilosplace.lib_java_carfax;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class CarfaxSearchResults {
	
	private Integer totalListingCount;
	private Integer enhancedCount;
	private Integer backfillCount;
	private Integer dealerNewCount;
	private Integer dealerUsedCount;
	private Integer page;
	private Integer pageSize;
	private Integer totalPageCount;
	
	private List<CarfaxListing> listings;
	
}

package net.nilosplace.lib_java_carfax;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import si.mazi.rescu.ClientConfig;
import si.mazi.rescu.RestProxyFactory;

@Log4j2
@Getter @Setter
public class CarfaxAPI {
	
	private String zip;
	private String make;
	private String model;
	private Integer radius;
	private Boolean noAccidents;
	private Boolean oneOwner;
	private CarfaxRestInterface api;
	
	
	//https://www.carfax.com/api/vehicles?zip=04605&radius=500&make=Toyota&model=Camry&certified=false&oneOwner=true&noAccidents=true&page=64
	
	public CarfaxAPI() {
		//ClientConfig config = new ClientConfig();
		api = RestProxyFactory.createProxy(CarfaxRestInterface.class, "https://www.carfax.com");
	}
	
	
	public List<CarfaxListing> searchVehicles() {

		String accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
		String acceptEncoding = "gzip, deflate, br";
		String acceptLanguage = "en-US,en;q=0.9";
		String cacheControl = "no-cache";
		String pragma = "no-cache";
		String secFetchDest = "document";
		String secFetchMode = "navigate";
		String secFetchSite = "none";
		String secFetchUser = "?1";
		Integer upgradeInsecureRequests = 1;
		String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36";

		CarfaxSearchResults results = api.vehicleSearch(
				accept, acceptEncoding, acceptLanguage, cacheControl, pragma, secFetchDest, secFetchMode,
				secFetchSite, secFetchUser, upgradeInsecureRequests, userAgent,
				zip, radius, make, model, noAccidents, oneOwner, 1);
		log.debug("Found: " + results.getTotalPageCount() + " pages retriving all listings.");
		
		for(int i = 2; i < results.getTotalPageCount(); i++) {
			CarfaxSearchResults res = api.vehicleSearch(
					accept, acceptEncoding, acceptLanguage, cacheControl, pragma, secFetchDest, secFetchMode,
					secFetchSite, secFetchUser, upgradeInsecureRequests, userAgent,
					zip, radius, make, model, noAccidents, oneOwner, i);
			results.getListings().addAll(res.getListings());
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return results.getListings();
		
	}
	
}
